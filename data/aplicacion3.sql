﻿DROP DATABASE IF EXISTS aplicacion3;
CREATE DATABASE aplicacion3;
USE aplicacion3;
DROP TABLE IF EXISTS coches;
CREATE TABLE coches(
id char(10),
marca varchar(50),
fecha date,
precio float,
PRIMARY KEY (id)
);
DROP TABLE IF EXISTS clientes;
CREATE TABLE clientes(
cod int,
nombre varchar(50),
idCocheAlquilado int,
fechaAlquiler date,
PRIMARY KEY (cod),
UNIQUE KEY (idCocheAlquilado)
);

ALTER TABLE clientes ADD CONSTRAINT fkClientesCoches
FOREIGN KEY (idCocheAlquilado)
REFERENCES coches(id) ON DELETE CASCADE ON UPDATE CASCADE;

